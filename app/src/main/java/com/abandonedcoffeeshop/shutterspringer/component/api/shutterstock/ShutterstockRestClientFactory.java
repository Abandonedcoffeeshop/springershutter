package com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock;

import com.abandonedcoffeeshop.shutterspringer.Constants;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

public class ShutterstockRestClientFactory {

    private static final String SHUTTERSTOCK_ENDPOINT = "https://api.shutterstock.com/v2";

    private static ShutterstockService mRestClient;

    public static ShutterstockService get() {
        if (mRestClient == null) {
            mRestClient = buildRestClient();
        }
        return mRestClient;
    }

    private static ShutterstockService buildRestClient() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(SHUTTERSTOCK_ENDPOINT)
                .setRequestInterceptor(buildRequestInterceptor())
                .build();
        return restAdapter.create(ShutterstockService.class);
    }

    private static RequestInterceptor buildRequestInterceptor() {
        return new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Authorization", "basic " + Constants.SHUTTERSTOCK_AUTH);
            }
        };
    }
}
