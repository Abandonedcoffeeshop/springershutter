package com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock;

import com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.model.ShutterstockImagesSearchResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface ShutterstockService {

    @GET("/images/search")
    void imageSearch(
            @Query("query") String query,
            Callback<ShutterstockImagesSearchResponse> callback
    );
}
