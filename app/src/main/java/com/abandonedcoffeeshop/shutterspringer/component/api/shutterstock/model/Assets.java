package com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.model;

import com.google.gson.annotations.SerializedName;

public class Assets {

    @SerializedName("preview")
    public imageData preview;

    @SerializedName("small_thumb")
    public imageData smallThubnail;

    @SerializedName("large_thumb")
    public imageData largeThumbnail;
}
