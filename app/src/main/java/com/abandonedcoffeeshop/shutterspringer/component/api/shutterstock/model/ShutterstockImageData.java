package com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.model;

import com.google.gson.annotations.SerializedName;

public class ShutterstockImageData {

    @SerializedName("id")
    public String id;

    @SerializedName("aspect")
    public float aspect;

    @SerializedName("assets")
    public Assets assets;

    @SerializedName("description")
    public String description;

}
