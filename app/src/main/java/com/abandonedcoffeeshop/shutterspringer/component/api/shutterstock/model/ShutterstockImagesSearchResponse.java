package com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShutterstockImagesSearchResponse {

    @SerializedName("page")
    public int page;

    @SerializedName("per_page")
    public int imagesPerPage;

    @SerializedName("total_count")
    public int totalImages;

    @SerializedName("search_id")
    public String searchID;

    @SerializedName("data")
    public List<ShutterstockImageData> shutterstockImageDataList;
}
