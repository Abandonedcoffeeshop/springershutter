package com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.model;

import com.google.gson.annotations.SerializedName;

public class imageData {

    @SerializedName("height")
    public int height;

    @SerializedName("width")
    public int width;

    @SerializedName("url")
    public String url;

}
