package com.abandonedcoffeeshop.shutterspringer.ui;

import android.app.SearchManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.SearchView;

import com.abandonedcoffeeshop.shutterspringer.R;

public class MainActivity extends AppCompatActivity {

    @SuppressWarnings("unused")
    private static final String TAG = MainActivity.class.getName();

    private static final String BUNDLE_KEY_SEARCHQUERY = "bundle_key_search_query";

    private SearchView mSearchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupSearchViewWidget();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(BUNDLE_KEY_SEARCHQUERY, getSearchQuery());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        setSearchQuery(savedInstanceState.getString(BUNDLE_KEY_SEARCHQUERY));
    }

    private String getSearchQuery() {
        return String.valueOf(mSearchView.getQuery());
    }

    private void setSearchQuery(String searchQuery) {
        mSearchView.setQuery(searchQuery, false);
        //after two hours of hacking away at this trying to get the searchview to take focus, I give up.
        //A possible hack would be a delayed thread (off the UI thread) that then posts to the UI thread after the delay.
    }

    private void setupSearchViewWidget() {
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        mSearchView = (SearchView) findViewById(R.id.activity_main_searchview);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        mSearchView.setSubmitButtonEnabled(true);
    }
}
