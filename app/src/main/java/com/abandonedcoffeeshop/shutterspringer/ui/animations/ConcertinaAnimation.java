package com.abandonedcoffeeshop.shutterspringer.ui.animations;

import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.abandonedcoffeeshop.shutterspringer.Constants;

public class ConcertinaAnimation extends Animation {
    @SuppressWarnings("unused")
    private static final String TAG = ConcertinaAnimation.class.getName();

    private View mViewToAnimate;
    private boolean mIsScrollingUp;


    public ConcertinaAnimation(View viewToAnimate, int measuredWidth, int measuredHeight, boolean isScrollingUp) {
        super();
        this.mViewToAnimate = viewToAnimate;
        this.setDuration(Constants.ANIMATION_DURATION);
        this.setInterpolator(new AccelerateDecelerateInterpolator());
        this.mIsScrollingUp = isScrollingUp;

        this.mViewToAnimate.setPivotX(measuredWidth / 2);

        if (isScrollingUp) {
            mViewToAnimate.setPivotY(measuredHeight);
        } else {
            mViewToAnimate.setPivotY(0);
        }
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        if (mIsScrollingUp) {
            mViewToAnimate.setRotationX(90 - (interpolatedTime * 90));
        } else {
            mViewToAnimate.setRotationX(270 + (interpolatedTime * 90));
        }
    }
}
