package com.abandonedcoffeeshop.shutterspringer.ui.searchresults;

import android.app.SearchManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.abandonedcoffeeshop.shutterspringer.Constants;
import com.abandonedcoffeeshop.shutterspringer.R;
import com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.ShutterstockRestClientFactory;
import com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.ShutterstockService;
import com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.model.ShutterstockImagesSearchResponse;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SearchResultsActivity extends AppCompatActivity {

    @SuppressWarnings("unused")
    private static final String TAG = SearchResultsActivity.class.getName();

    private ShutterstockService mShutterstockAPI;

    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private TextView mErrorTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchresults);
        setupProgressBar();
        setupToolbar();
        setupRecyclerView();
        setupErrorTextView();
        requestShutterstockImages();
    }

    public void setAPI(ShutterstockService mockAPIService) {
        mShutterstockAPI = mockAPIService;
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public String getCurrentErrorString() {
        return String.valueOf(mErrorTextView.getText());
    }

    private ShutterstockService getAPI() {
        if (mShutterstockAPI == null) {
            mShutterstockAPI = ShutterstockRestClientFactory.get();
        }
        return mShutterstockAPI;
    }

    private void setupErrorTextView() {
        mErrorTextView = (TextView) findViewById(R.id.activity_searchresults_errorview);
    }

    private void setupProgressBar() {
        mProgressBar = (ProgressBar) findViewById(R.id.activity_searchresults_progressbar);
    }

    private void setupToolbar() {
        TextView toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        String toolbarTitleString = String.format(getString(R.string.activity_searchresults_toolbartitle), getSearchQuery());
        toolbarTitle.setText(toolbarTitleString);
    }

    private void setupRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.activity_searchresults_recylcerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private String getSearchQuery() {
        return getIntent().getStringExtra(SearchManager.QUERY);
    }

    private void requestShutterstockImages() {
        getAPI().imageSearch(getSearchQuery(), mShutterstockImageQueryCallback);
    }

    private void updateActivityWithValidNetworkResponse(ShutterstockImagesSearchResponse shutterstockImagesSearchResponse) {
        if (shutterstockImagesSearchResponse == null || shutterstockImagesSearchResponse.totalImages == 0) {
            showErrorNoContent();
        } else {
            showRecyclerViewWithContent(shutterstockImagesSearchResponse);
        }
    }

    private void showRecyclerViewWithContent(ShutterstockImagesSearchResponse shutterstockImagesSearchResponse) {
        mRecyclerView.setAdapter(new SearchResultsRecyclerViewAdapter(mRecyclerView, shutterstockImagesSearchResponse.shutterstockImageDataList));
        mRecyclerView.animate().alpha(1f).setDuration(Constants.ANIMATION_DURATION).start();
    }

    private void showErrorNetworkFail() {
        mErrorTextView.setText(getString(R.string.activity_searchresults_error_network));
        mErrorTextView.animate().alpha(1).setDuration(Constants.ANIMATION_DURATION).start();
    }

    private void showErrorNoContent() {
        mErrorTextView.setText(getString(R.string.activity_searchresults_error_nocontent));
        mErrorTextView.animate().alpha(1).setDuration(Constants.ANIMATION_DURATION).start();
    }

    private void hideProgressBar() {
        mProgressBar.animate().alpha(0f).setDuration(Constants.ANIMATION_DURATION).start();
    }

    private Callback<ShutterstockImagesSearchResponse> mShutterstockImageQueryCallback = new Callback<ShutterstockImagesSearchResponse>() {
        @Override
        public void success(ShutterstockImagesSearchResponse shutterstockImagesSearchResponse, Response response) {
            Log.e(TAG, "Success");
            hideProgressBar();
            updateActivityWithValidNetworkResponse(shutterstockImagesSearchResponse);
        }

        @Override
        public void failure(RetrofitError error) {
            String errorMessage = error == null ? "NO ERROR MESSAGE" : error.getMessage();
            Log.e(TAG, "Failure: " + errorMessage);
            hideProgressBar();
            showErrorNetworkFail();
        }
    };
}
