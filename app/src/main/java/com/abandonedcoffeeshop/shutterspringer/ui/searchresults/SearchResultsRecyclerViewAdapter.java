package com.abandonedcoffeeshop.shutterspringer.ui.searchresults;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abandonedcoffeeshop.shutterspringer.R;
import com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.model.ShutterstockImageData;

import java.util.List;

public class SearchResultsRecyclerViewAdapter extends RecyclerView.Adapter<SearchResultsViewHolder> {

    @SuppressWarnings("unused")
    private static final String TAG = SearchResultsRecyclerViewAdapter.class.getName();

    private List<ShutterstockImageData> mShutterstockImageDataList;
    private RecyclerView mRecyclerView;
    private boolean isScrollingUp;

    public SearchResultsRecyclerViewAdapter(RecyclerView recyclerView, List<ShutterstockImageData> mShutterstockImageDataList) {
        this.mShutterstockImageDataList = mShutterstockImageDataList;
        this.mRecyclerView = recyclerView;
        createScrollListenerForRecyclerView();
    }

    private void createScrollListenerForRecyclerView() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                isScrollingUp = dy < 0;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mShutterstockImageDataList.size();
    }

    @Override
    public SearchResultsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.listitem_searchresult, parent, false);
        return new SearchResultsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchResultsViewHolder holder, int position) {

        holder.bindData(mShutterstockImageDataList.get(position), mRecyclerView, isScrollingUp);
    }
}
