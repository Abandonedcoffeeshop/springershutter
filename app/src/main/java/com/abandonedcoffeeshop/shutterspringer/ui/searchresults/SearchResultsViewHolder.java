package com.abandonedcoffeeshop.shutterspringer.ui.searchresults;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.abandonedcoffeeshop.shutterspringer.Constants;
import com.abandonedcoffeeshop.shutterspringer.R;
import com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.model.ShutterstockImageData;
import com.abandonedcoffeeshop.shutterspringer.ui.animations.ConcertinaAnimation;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class SearchResultsViewHolder extends RecyclerView.ViewHolder {

    @SuppressWarnings("unused")
    private static final String TAG = SearchResultsViewHolder.class.getName();

    private ImageView mImageView;
    private ProgressBar mProgressBar;
    private View mParentView;
    private int mWindowWidth;

    public SearchResultsViewHolder(View itemView) {
        super(itemView);

        mImageView = (ImageView) itemView.findViewById(R.id.listitem_searchresult_imageview);
        mProgressBar = (ProgressBar) itemView.findViewById(R.id.listitem_searchresult_progressbar);
        mParentView = itemView;
        mWindowWidth = getWindowWidth();
    }

    private int getWindowWidth() {
        WindowManager wm = (WindowManager) mParentView.getContext().getSystemService(Context.WINDOW_SERVICE);
        Point point = new Point();
        Display display = wm.getDefaultDisplay();
        display.getSize(point);
        return point.x;
    }

    public void bindData(ShutterstockImageData imageData, RecyclerView recyclerView, boolean isScrollingUp) {
        requestImageForImageView(imageData.assets.preview.url, recyclerView.getContext());
        startAnimation(isScrollingUp);
    }

    private void startAnimation(boolean isScrollingUp) {
        int height = mParentView.getLayoutParams().height;
        Animation concertinaAnimation = new ConcertinaAnimation(mParentView, mWindowWidth, height, isScrollingUp);
        mParentView.startAnimation(concertinaAnimation);
    }

    private void requestImageForImageView(String url, Context context) {
        Picasso.with(context).load(url).into(mImageView, mPicassoImageLoadCallback);
    }


    private Callback mPicassoImageLoadCallback = new Callback() {
        @Override
        public void onSuccess() {
            mImageView.animate().alpha(1).setDuration(Constants.ANIMATION_DURATION).start();
            mProgressBar.animate().alpha(0).setDuration(Constants.ANIMATION_DURATION).start();
        }

        @Override
        public void onError() {

        }
    };
}
