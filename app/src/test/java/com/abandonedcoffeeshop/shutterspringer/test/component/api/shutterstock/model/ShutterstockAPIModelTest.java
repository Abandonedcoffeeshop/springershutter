package com.abandonedcoffeeshop.shutterspringer.test.component.api.shutterstock.model;

import com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.model.ShutterstockImagesSearchResponse;
import com.abandonedcoffeeshop.shutterspringer.test.factories.APIDataStringFactory;
import com.google.gson.Gson;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ShutterstockAPIModelTest {

    @Test
    public void testSanityCheck() {
        assertEquals("Compiler fail. Check for butterflies", 1, 1);
    }

    @Test
    public void testCanInstantiateModelClass() {
        ShutterstockImagesSearchResponse classUnderTest = new ShutterstockImagesSearchResponse();
        assertNotNull(classUnderTest);
    }

    @Test
    public void testGSONDeserialisesModelWithValidDummyData_NonNull() {
        String dummyData = APIDataStringFactory.dummyResponseForImagesSearchWithEmptyInput;
        Gson gson = new Gson();
        ShutterstockImagesSearchResponse classUnderTest = gson.fromJson(dummyData, ShutterstockImagesSearchResponse.class);
        assertNotNull(classUnderTest);
    }

    @Test
    public void testGsonDeserialisesModelWithValidDummyData_CorrectValues() {
        String dummyData = APIDataStringFactory.dummyResponseForImagesSearchWithEmptyInput;
        Gson gson = new Gson();
        ShutterstockImagesSearchResponse classUnderTest = gson.fromJson(dummyData, ShutterstockImagesSearchResponse.class);

        String expectedValue = "284856695";
        String actualValue = classUnderTest.shutterstockImageDataList.get(0).id;

        assertEquals("Model did not deserialise correctly. Has someone changed the model and not the test data? ", expectedValue, actualValue);
    }

}
