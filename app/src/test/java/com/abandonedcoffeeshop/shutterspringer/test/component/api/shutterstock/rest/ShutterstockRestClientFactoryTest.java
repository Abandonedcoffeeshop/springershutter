package com.abandonedcoffeeshop.shutterspringer.test.component.api.shutterstock.rest;

import com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.ShutterstockRestClientFactory;
import com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.ShutterstockService;
import com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.model.ShutterstockImagesSearchResponse;

import org.junit.Test;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static org.junit.Assert.assertNotNull;

public class ShutterstockRestClientFactoryTest {

    @Test
    public void testShutterstockRestClientDoesNotThrowException() {
        ShutterstockRestClientFactory.get();
    }

    @Test
    public void testShutterstockRestClientReturnsNonNullObject() {
        assertNotNull(ShutterstockRestClientFactory.get());
    }

    @Test
    public void testShutterstockRestClientDoesNotThrowOnNullSearchQuery() {
        ShutterstockService service = ShutterstockRestClientFactory.get();
        service.imageSearch(null, new Callback<ShutterstockImagesSearchResponse>() {
            @Override
            public void success(ShutterstockImagesSearchResponse shutterstockImagesSearchResponse, Response response) {
                //Ignore
            }

            @Override
            public void failure(RetrofitError error) {
                //Ignore
            }
        });
    }

    @Test
    public void testShutterstockRestClientDoesNotThrowOnNullCallback() {
        ShutterstockRestClientFactory.get().imageSearch("Lemons", null);
    }

    @Test
    public void testShutterstockRestClientDoesNotThrowOnNullEverything() {
        ShutterstockRestClientFactory.get().imageSearch(null, null);
    }
}
