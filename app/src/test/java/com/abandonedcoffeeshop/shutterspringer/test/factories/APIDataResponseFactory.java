package com.abandonedcoffeeshop.shutterspringer.test.factories;

import com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.model.ShutterstockImagesSearchResponse;
import com.google.gson.Gson;

public class APIDataResponseFactory {

    public static ShutterstockImagesSearchResponse createValidResponse() {
        Gson gson = new Gson();
        return gson.fromJson(APIDataStringFactory.dummyResponseForImagesSearchWithEmptyInput, ShutterstockImagesSearchResponse.class);
    }
}
