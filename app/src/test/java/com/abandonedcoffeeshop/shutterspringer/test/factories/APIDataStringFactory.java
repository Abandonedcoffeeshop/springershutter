package com.abandonedcoffeeshop.shutterspringer.test.factories;

public class APIDataStringFactory {

    public static String dummyResponseForImagesSearchWithEmptyInput = "{\n" +
            "  \"page\": 1,\n" +
            "  \"per_page\": 20,\n" +
            "  \"total_count\": 54671295,\n" +
            "  \"search_id\": \"u_auTAJ_xhwy_fKhsMpdrw\",\n" +
            "  \"data\": [\n" +
            "    {\n" +
            "      \"id\": \"284856695\",\n" +
            "      \"aspect\": 0.9979,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 450,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/675421/284856695/stock-vector-ramadan-kareem-greeting-background-eps-284856695.jpg\",\n" +
            "          \"width\": 449\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 100,\n" +
            "          \"url\": \"http://thumb7.shutterstock.com/thumb_small/675421/284856695/stock-vector-ramadan-kareem-greeting-background-eps-284856695.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 150,\n" +
            "          \"url\": \"http://thumb7.shutterstock.com/thumb_large/675421/284856695/stock-vector-ramadan-kareem-greeting-background-eps-284856695.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"675421\"\n" +
            "      },\n" +
            "      \"description\": \"Ramadan Kareem, greeting background, eps 10\",\n" +
            "      \"image_type\": \"vector\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"281158268\",\n" +
            "      \"aspect\": 1,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 450,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/2430962/281158268/stock-vector-red-ribbon-with-title-for-brazil-june-party-vector-illustration-281158268.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 100,\n" +
            "          \"url\": \"http://thumb1.shutterstock.com/thumb_small/2430962/281158268/stock-vector-red-ribbon-with-title-for-brazil-june-party-vector-illustration-281158268.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 150,\n" +
            "          \"url\": \"http://thumb1.shutterstock.com/thumb_large/2430962/281158268/stock-vector-red-ribbon-with-title-for-brazil-june-party-vector-illustration-281158268.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"2430962\"\n" +
            "      },\n" +
            "      \"description\": \"Red ribbon with title for Brazil june party. Vector illustration.\",\n" +
            "      \"image_type\": \"vector\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"289247882\",\n" +
            "      \"aspect\": 0.9969,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 450,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/675421/289247882/stock-vector-eid-mubarak-greeting-background-eps-289247882.jpg\",\n" +
            "          \"width\": 448\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 100,\n" +
            "          \"url\": \"http://thumb9.shutterstock.com/thumb_small/675421/289247882/stock-vector-eid-mubarak-greeting-background-eps-289247882.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 150,\n" +
            "          \"url\": \"http://thumb9.shutterstock.com/thumb_large/675421/289247882/stock-vector-eid-mubarak-greeting-background-eps-289247882.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"675421\"\n" +
            "      },\n" +
            "      \"description\": \"Eid Mubarak, greeting background, eps 10\",\n" +
            "      \"image_type\": \"vector\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"275666651\",\n" +
            "      \"aspect\": 1.4979,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 300,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/828115/275666651/stock-photo-two-business-people-shaking-hands-as-successful-agreement-in-real-estate-agency-office-concept-of-275666651.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 67,\n" +
            "          \"url\": \"http://thumb7.shutterstock.com/thumb_small/828115/275666651/stock-photo-two-business-people-shaking-hands-as-successful-agreement-in-real-estate-agency-office-concept-of-275666651.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 100,\n" +
            "          \"url\": \"http://thumb7.shutterstock.com/thumb_large/828115/275666651/stock-photo-two-business-people-shaking-hands-as-successful-agreement-in-real-estate-agency-office-concept-of-275666651.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"828115\"\n" +
            "      },\n" +
            "      \"description\": \"Two business people shaking hands as successful agreement in real estate agency office. Concept of housing purchase and insurance. \",\n" +
            "      \"image_type\": \"photo\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"273096269\",\n" +
            "      \"aspect\": 1.5455,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 291,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/559519/273096269/stock-photo-young-mother-in-home-office-with-computer-and-her-daugher-273096269.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 65,\n" +
            "          \"url\": \"http://thumb7.shutterstock.com/thumb_small/559519/273096269/stock-photo-young-mother-in-home-office-with-computer-and-her-daugher-273096269.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 97,\n" +
            "          \"url\": \"http://thumb7.shutterstock.com/thumb_large/559519/273096269/stock-photo-young-mother-in-home-office-with-computer-and-her-daugher-273096269.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"559519\"\n" +
            "      },\n" +
            "      \"description\": \"Young mother in home office with computer and her daugher\",\n" +
            "      \"image_type\": \"photo\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"275161592\",\n" +
            "      \"aspect\": 1.5964,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 281,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/162265/275161592/stock-photo-female-hands-with-pen-writing-on-notebook-275161592.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 63,\n" +
            "          \"url\": \"http://thumb9.shutterstock.com/thumb_small/162265/275161592/stock-photo-female-hands-with-pen-writing-on-notebook-275161592.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 94,\n" +
            "          \"url\": \"http://thumb9.shutterstock.com/thumb_large/162265/275161592/stock-photo-female-hands-with-pen-writing-on-notebook-275161592.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"162265\"\n" +
            "      },\n" +
            "      \"description\": \"female hands with pen writing on notebook\",\n" +
            "      \"image_type\": \"photo\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"281075888\",\n" +
            "      \"aspect\": 1,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 450,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/1288123/281075888/stock-vector-you-can-do-it-motivation-square-acrylic-stroke-poster-text-lettering-of-an-inspirational-saying-281075888.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 100,\n" +
            "          \"url\": \"http://thumb1.shutterstock.com/thumb_small/1288123/281075888/stock-vector-you-can-do-it-motivation-square-acrylic-stroke-poster-text-lettering-of-an-inspirational-saying-281075888.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 150,\n" +
            "          \"url\": \"http://thumb1.shutterstock.com/thumb_large/1288123/281075888/stock-vector-you-can-do-it-motivation-square-acrylic-stroke-poster-text-lettering-of-an-inspirational-saying-281075888.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"1288123\"\n" +
            "      },\n" +
            "      \"description\": \"You Can Do It motivation square acrylic stroke poster. Text lettering of an inspirational saying. Quote Typographical Poster Template, vector design\",\n" +
            "      \"image_type\": \"vector\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"268450487\",\n" +
            "      \"aspect\": 1.5,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 300,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/2324765/268450487/stock-photo-side-view-shot-of-a-man-s-hands-using-smart-phone-in-interior-rear-view-of-business-man-hands-busy-268450487.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 67,\n" +
            "          \"url\": \"http://thumb10.shutterstock.com/thumb_small/2324765/268450487/stock-photo-side-view-shot-of-a-man-s-hands-using-smart-phone-in-interior-rear-view-of-business-man-hands-busy-268450487.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 100,\n" +
            "          \"url\": \"http://thumb10.shutterstock.com/thumb_large/2324765/268450487/stock-photo-side-view-shot-of-a-man-s-hands-using-smart-phone-in-interior-rear-view-of-business-man-hands-busy-268450487.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"2324765\"\n" +
            "      },\n" +
            "      \"description\": \"Side view shot of a man's hands using smart phone in interior, rear view of business man hands busy using cell phone at office desk, young male student typing on phone sitting at wooden table, flare\",\n" +
            "      \"image_type\": \"photo\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"268450493\",\n" +
            "      \"aspect\": 1.5,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 300,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/2324765/268450493/stock-photo-silhouette-of-cropped-shot-of-a-young-man-working-from-home-using-smart-phone-and-notebook-computer-268450493.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 67,\n" +
            "          \"url\": \"http://thumb10.shutterstock.com/thumb_small/2324765/268450493/stock-photo-silhouette-of-cropped-shot-of-a-young-man-working-from-home-using-smart-phone-and-notebook-computer-268450493.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 100,\n" +
            "          \"url\": \"http://thumb10.shutterstock.com/thumb_large/2324765/268450493/stock-photo-silhouette-of-cropped-shot-of-a-young-man-working-from-home-using-smart-phone-and-notebook-computer-268450493.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"2324765\"\n" +
            "      },\n" +
            "      \"description\": \"Silhouette of cropped shot of a young man working from home using smart phone and notebook computer, man's hands using smart phone in interior, man at his workplace using technology, flare light\",\n" +
            "      \"image_type\": \"photo\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"271745117\",\n" +
            "      \"aspect\": 1.447,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 310,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/1428518/271745117/stock-photo-young-hipster-fashion-guy-with-computer-tablet-sitting-next-his-car-during-road-trip-concept-of-271745117.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 69,\n" +
            "          \"url\": \"http://thumb10.shutterstock.com/thumb_small/1428518/271745117/stock-photo-young-hipster-fashion-guy-with-computer-tablet-sitting-next-his-car-during-road-trip-concept-of-271745117.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 104,\n" +
            "          \"url\": \"http://thumb10.shutterstock.com/thumb_large/1428518/271745117/stock-photo-young-hipster-fashion-guy-with-computer-tablet-sitting-next-his-car-during-road-trip-concept-of-271745117.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"1428518\"\n" +
            "      },\n" +
            "      \"description\": \"Young hipster fashion guy with computer tablet sitting next his car during road trip - Concept of new trends and technology mixed with vintage lifestyle - Traveler man on retro nostalgic filtered look\",\n" +
            "      \"image_type\": \"photo\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"275562389\",\n" +
            "      \"aspect\": 1.5286,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 294,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/757165/275562389/stock-photo-father-and-son-sitting-under-the-tree-on-spring-lawn-275562389.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 65,\n" +
            "          \"url\": \"http://thumb7.shutterstock.com/thumb_small/757165/275562389/stock-photo-father-and-son-sitting-under-the-tree-on-spring-lawn-275562389.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 98,\n" +
            "          \"url\": \"http://thumb7.shutterstock.com/thumb_large/757165/275562389/stock-photo-father-and-son-sitting-under-the-tree-on-spring-lawn-275562389.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"757165\"\n" +
            "      },\n" +
            "      \"description\": \"father and son sitting under the tree on spring lawn\",\n" +
            "      \"image_type\": \"photo\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"279448916\",\n" +
            "      \"aspect\": 1.4908,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 301,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/2733490/279448916/stock-vector-vector-set-retro-neon-sign-vintage-billboard-bright-signboard-light-banner-279448916.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 67,\n" +
            "          \"url\": \"http://thumb9.shutterstock.com/thumb_small/2733490/279448916/stock-vector-vector-set-retro-neon-sign-vintage-billboard-bright-signboard-light-banner-279448916.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 101,\n" +
            "          \"url\": \"http://thumb9.shutterstock.com/thumb_large/2733490/279448916/stock-vector-vector-set-retro-neon-sign-vintage-billboard-bright-signboard-light-banner-279448916.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"2733490\"\n" +
            "      },\n" +
            "      \"description\": \"Vector set Retro neon sign, vintage billboard, bright signboard, light banner \",\n" +
            "      \"image_type\": \"vector\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"267153857\",\n" +
            "      \"aspect\": 1.1426,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 393,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/1293919/267153857/stock-vector-set-luxury-logos-template-flourishes-calligraphic-elegant-ornament-lines-business-sign-identity-267153857.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 88,\n" +
            "          \"url\": \"http://thumb10.shutterstock.com/thumb_small/1293919/267153857/stock-vector-set-luxury-logos-template-flourishes-calligraphic-elegant-ornament-lines-business-sign-identity-267153857.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 131,\n" +
            "          \"url\": \"http://thumb10.shutterstock.com/thumb_large/1293919/267153857/stock-vector-set-luxury-logos-template-flourishes-calligraphic-elegant-ornament-lines-business-sign-identity-267153857.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"1293919\"\n" +
            "      },\n" +
            "      \"description\": \"Set Luxury Logos template flourishes calligraphic elegant ornament lines. Business sign, identity for Restaurant, Royalty, Boutique, Hotel, Heraldic, Jewelry, Fashion and other vector illustration\",\n" +
            "      \"image_type\": \"vector\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"270938732\",\n" +
            "      \"aspect\": 1.5,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 300,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/1020994/270938732/stock-photo-euphoric-woman-watching-her-smart-phone-in-a-train-station-while-is-waiting-270938732.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 67,\n" +
            "          \"url\": \"http://thumb9.shutterstock.com/thumb_small/1020994/270938732/stock-photo-euphoric-woman-watching-her-smart-phone-in-a-train-station-while-is-waiting-270938732.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 100,\n" +
            "          \"url\": \"http://thumb9.shutterstock.com/thumb_large/1020994/270938732/stock-photo-euphoric-woman-watching-her-smart-phone-in-a-train-station-while-is-waiting-270938732.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"1020994\"\n" +
            "      },\n" +
            "      \"description\": \"Euphoric woman watching her smart phone in a train station while is waiting \",\n" +
            "      \"image_type\": \"photo\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"274566236\",\n" +
            "      \"aspect\": 1.3358,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 336,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/1454597/274566236/stock-photo-crystal-globe-resting-on-moss-in-a-forest-environment-concept-274566236.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 75,\n" +
            "          \"url\": \"http://thumb9.shutterstock.com/thumb_small/1454597/274566236/stock-photo-crystal-globe-resting-on-moss-in-a-forest-environment-concept-274566236.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 112,\n" +
            "          \"url\": \"http://thumb9.shutterstock.com/thumb_large/1454597/274566236/stock-photo-crystal-globe-resting-on-moss-in-a-forest-environment-concept-274566236.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"1454597\"\n" +
            "      },\n" +
            "      \"description\": \"crystal globe resting on moss in a forest - environment concept\\n\",\n" +
            "      \"image_type\": \"photo\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"282200660\",\n" +
            "      \"aspect\": 1.3744,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 327,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/404329/282200660/stock-vector-ramadan-celebration-vintage-engraved-illustration-hand-drawn-282200660.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 73,\n" +
            "          \"url\": \"http://thumb1.shutterstock.com/thumb_small/404329/282200660/stock-vector-ramadan-celebration-vintage-engraved-illustration-hand-drawn-282200660.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 109,\n" +
            "          \"url\": \"http://thumb1.shutterstock.com/thumb_large/404329/282200660/stock-vector-ramadan-celebration-vintage-engraved-illustration-hand-drawn-282200660.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"404329\"\n" +
            "      },\n" +
            "      \"description\": \"Ramadan celebration vintage engraved illustration, hand drawn\",\n" +
            "      \"image_type\": \"vector\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"281019797\",\n" +
            "      \"aspect\": 0.7071,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 450,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/3114101/281019797/stock-vector-latin-american-holiday-the-june-party-of-brazil-bright-night-the-background-with-colonial-houses-281019797.jpg\",\n" +
            "          \"width\": 318\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 100,\n" +
            "          \"url\": \"http://thumb10.shutterstock.com/thumb_small/3114101/281019797/stock-vector-latin-american-holiday-the-june-party-of-brazil-bright-night-the-background-with-colonial-houses-281019797.jpg\",\n" +
            "          \"width\": 71\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 150,\n" +
            "          \"url\": \"http://thumb10.shutterstock.com/thumb_large/3114101/281019797/stock-vector-latin-american-holiday-the-june-party-of-brazil-bright-night-the-background-with-colonial-houses-281019797.jpg\",\n" +
            "          \"width\": 106\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"3114101\"\n" +
            "      },\n" +
            "      \"description\": \"Latin American holiday, the June party of Brazil, bright night the background with colonial houses, church, lights and colored flags and the words in Portuguese Festa Junina Vector\",\n" +
            "      \"image_type\": \"vector\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"270834446\",\n" +
            "      \"aspect\": 1.5,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 300,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/449008/270834446/stock-photo-happy-family-playing-in-swimming-pool-summer-vacation-concept-270834446.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 67,\n" +
            "          \"url\": \"http://thumb9.shutterstock.com/thumb_small/449008/270834446/stock-photo-happy-family-playing-in-swimming-pool-summer-vacation-concept-270834446.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 100,\n" +
            "          \"url\": \"http://thumb9.shutterstock.com/thumb_large/449008/270834446/stock-photo-happy-family-playing-in-swimming-pool-summer-vacation-concept-270834446.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"449008\"\n" +
            "      },\n" +
            "      \"description\": \"Happy family playing in swimming pool. Summer vacation concept\",\n" +
            "      \"image_type\": \"photo\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"280221953\",\n" +
            "      \"aspect\": 1.3806,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 325,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/1383694/280221953/stock-vector-traditional-lantern-of-ramadan-ramadan-kareem-beautiful-greeting-card-with-arabic-calligraphy-280221953.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 72,\n" +
            "          \"url\": \"http://thumb10.shutterstock.com/thumb_small/1383694/280221953/stock-vector-traditional-lantern-of-ramadan-ramadan-kareem-beautiful-greeting-card-with-arabic-calligraphy-280221953.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 109,\n" +
            "          \"url\": \"http://thumb10.shutterstock.com/thumb_large/1383694/280221953/stock-vector-traditional-lantern-of-ramadan-ramadan-kareem-beautiful-greeting-card-with-arabic-calligraphy-280221953.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"1383694\"\n" +
            "      },\n" +
            "      \"description\": \"traditional lantern of Ramadan- Ramadan Kareem beautiful greeting card with arabic calligraphy which means ''Ramadan kareem '' .\",\n" +
            "      \"image_type\": \"vector\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"277455644\",\n" +
            "      \"aspect\": 1.5,\n" +
            "      \"assets\": {\n" +
            "        \"preview\": {\n" +
            "          \"height\": 300,\n" +
            "          \"url\": \"http://image.shutterstock.com/display_pic_with_logo/449008/277455644/stock-photo-flip-flops-beach-ball-and-snorkel-on-the-sand-summer-vacation-concept-277455644.jpg\",\n" +
            "          \"width\": 450\n" +
            "        },\n" +
            "        \"small_thumb\": {\n" +
            "          \"height\": 67,\n" +
            "          \"url\": \"http://thumb1.shutterstock.com/thumb_small/449008/277455644/stock-photo-flip-flops-beach-ball-and-snorkel-on-the-sand-summer-vacation-concept-277455644.jpg\",\n" +
            "          \"width\": 100\n" +
            "        },\n" +
            "        \"large_thumb\": {\n" +
            "          \"height\": 100,\n" +
            "          \"url\": \"http://thumb1.shutterstock.com/thumb_large/449008/277455644/stock-photo-flip-flops-beach-ball-and-snorkel-on-the-sand-summer-vacation-concept-277455644.jpg\",\n" +
            "          \"width\": 150\n" +
            "        }\n" +
            "      },\n" +
            "      \"contributor\": {\n" +
            "        \"id\": \"449008\"\n" +
            "      },\n" +
            "      \"description\": \"Flip-flops, beach ball and snorkel on the sand. Summer vacation concept\",\n" +
            "      \"image_type\": \"photo\",\n" +
            "      \"is_illustration\": false,\n" +
            "      \"media_type\": \"image\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";
}
