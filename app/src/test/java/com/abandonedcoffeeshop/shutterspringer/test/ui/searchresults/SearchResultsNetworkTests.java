package com.abandonedcoffeeshop.shutterspringer.test.ui.searchresults;

import com.abandonedcoffeeshop.shutterspringer.BuildConfig;
import com.abandonedcoffeeshop.shutterspringer.R;
import com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.ShutterstockService;
import com.abandonedcoffeeshop.shutterspringer.component.api.shutterstock.model.ShutterstockImagesSearchResponse;
import com.abandonedcoffeeshop.shutterspringer.test.factories.APIDataResponseFactory;
import com.abandonedcoffeeshop.shutterspringer.ui.searchresults.SearchResultsActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.util.ActivityController;

import retrofit.Callback;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 16)
public class SearchResultsNetworkTests {

    private SearchResultsActivity mClassUnderTest;

    @Mock
    private ShutterstockService mockAPIService;

    @Captor
    private ArgumentCaptor<Callback<ShutterstockImagesSearchResponse>> argumentCaptor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ActivityController<SearchResultsActivity> activityController = Robolectric.buildActivity(SearchResultsActivity.class);

        mClassUnderTest = activityController.get();
        mClassUnderTest.setAPI(mockAPIService);
        activityController.create();
    }

    @Test
    public void sanityTest() {
        assertTrue(1 == 1);
    }

    @Test
    public void assertActivityNotNull() {
        assertNotNull(mClassUnderTest);
    }

    @Test
    public void assertSearchResultsAreShownOnSuccessfulNetworkTransaction() {
        Mockito.verify(mockAPIService).imageSearch(Mockito.anyString(), argumentCaptor.capture());

        argumentCaptor.getValue().success(APIDataResponseFactory.createValidResponse(), null);

        int expectedValue = 20;
        int actualValue = mClassUnderTest.getRecyclerView().getAdapter().getItemCount();
        String error = "RecyclerView in activity did not return correct count of results";
        assertEquals(error, expectedValue, actualValue);
    }

    @Test
    public void assertErrorShownAfterNetworkTransactionError() {
        Mockito.verify(mockAPIService).imageSearch(Mockito.anyString(), argumentCaptor.capture());

        argumentCaptor.getValue().failure(null);

        String expectedValue = mClassUnderTest.getString(R.string.activity_searchresults_error_network);
        String actualValue = mClassUnderTest.getCurrentErrorString();
        String errorString = "SearchActivity did not display correct error on network fail";
        assertEquals(errorString, expectedValue, actualValue);
    }

    @Test
    public void assertCorrectMessageShownToUserWhenNullResultsReturnedFromAPI() {
        Mockito.verify(mockAPIService).imageSearch(Mockito.anyString(), argumentCaptor.capture());

        argumentCaptor.getValue().success(null, null);
        String expectedValue = mClassUnderTest.getString(R.string.activity_searchresults_error_nocontent);
        String actualValue = mClassUnderTest.getCurrentErrorString();
        String errorString = "SearchActivity did not display correct error on null API response";
        assertEquals(errorString, expectedValue, actualValue);
    }

    @Test
    public void assertCorrectMessageShownToUserWhenEmptyResultsReturnedFromAPI() {
        Mockito.verify(mockAPIService).imageSearch(Mockito.anyString(), argumentCaptor.capture());

        ShutterstockImagesSearchResponse response = new ShutterstockImagesSearchResponse();

        argumentCaptor.getValue().success(response, null);

        String expectedValue = mClassUnderTest.getString(R.string.activity_searchresults_error_nocontent);
        String actualValue = mClassUnderTest.getCurrentErrorString();
        String errorString = "SearchActivity did not return the correct error on empty API response";
        assertEquals(errorString, expectedValue, actualValue);
    }

}
